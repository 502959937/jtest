//
// Created by chenlei on 1/23/16.
//

#include "Pointer.h"
#include "outstrm.h"
#include <iostream>
#include <list>
#include <vector>
#include "outstrm.h"

using namespace std;

#ifndef SIZE
#define SIZE 1000
#endif

/**
 *
 */
void Pointer::ptr_println() {
    cout << "ptr_value:" << *ptr_int << endl;
    //reset value
    *ptr_int = 100;
//    sprintf"reset value:%d", (const char *) *ptr_int);

    int array[20] = {10, 21};
    //array 为 int数组的首地址
    int *ptr = &array[0];

    size_t length = sizeof(array) / sizeof(array[0]);

    for (int i = 0; i < length; ++i) {
        (*ptr)++;
        if (*ptr == 0)break;
//        printf("value: %d\n", *ptr);
        ptr++;
    }

    auto aslist = new list<int>;

    aslist->push_back(1);
    aslist->push_back(3);
    aslist->push_back(2);
    aslist->sort();


    //迭代输出
    for (auto iter = aslist->begin(); iter !=
                                      aslist->end(); ++iter) {
        printf("list value:%d\n", *iter);
    }


    //look some speacial data
    list<int>::iterator ptr_list = find(aslist->begin(), aslist->end(), 74);

    if (ptr_list == aslist->end()) {
        printf("not found 74\n");
    }


    /**
     *
     */
    list<int>::iterator _iter = aslist->begin();
    while (_iter != aslist->end()) {
        ++_iter;
        printf("iterator value:%d\n", *_iter);
    }


    //
    aslist->clear();
    delete aslist;
    aslist = nullptr;


    int itrer_array[SIZE];
    itrer_array[20] = 100;

    //遍历数组是否存在
    int *__first = find(itrer_array, itrer_array + SIZE, 12);
    //exsite
    if (__first == itrer_array + SIZE) {
        printf("not found value\n");
    } else {
        printf("found it:%d\n", *__first);
    }


    //迭代器遍历查找
    vector<int> intVector(10);
    intVector[10] = 50;
    vector<int>::iterator intIter =
            find(intVector.begin(), intVector.end(), 50);
    if (intIter == intVector.end()) {
        cout << "Vector contains value " << *intIter << endl;
        *intIter = 90;
        if (find(intVector.begin(), intVector.end(), 90) == intVector.end()) {
        } else {
            //found
        }
    }
    else {
        cout << "Vector does not contain 50" << endl;

    }


    // base stl copy

    double darray[10] =
            {1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9};

    vector<double> d_vector(10);
    //
    copy(darray, darray + 10, d_vector.begin());
    vector<double>::iterator temp = d_vector.begin();
    while (temp != d_vector.end()) {
        printf("temp:%f\n", *temp);
        ++temp;
    }
    printf("random_shuffle \n");
    //random shuffer
    random_shuffle(d_vector.begin(), d_vector.end());
    temp = d_vector.begin();
    while (temp != d_vector.end()) {
        printf("temp:%f\n", *temp);
        ++temp;
    }

    printf("\n");



    /**
     * 流操作
     */

    outstrm *outstrm1=new outstrm;
    // Seed the random number generator
    srandom(time(nullptr) );
    // Construct vector and fill with random integer values
    vector<long> collection(10);
    for (int i = 0; i < 10; i++)
        collection[i] = random() % 10000;;

    // Display, sort, and redisplay
    outstrm1->Display(collection, "Before sorting");
    sort(collection.begin(), collection.end());
    outstrm1->Display(collection, "After sorting");
}

/**6
 *
 */
void Pointer::ptr_test() {

}
