//
// Created by chenlei on 15/5/14.
//

#ifndef JTEST_PERSON_H
#define JTEST_PERSON_H


#include "Base.h"

/**
 *
 */
class Person : public Base {

public:

    virtual void say();

    virtual void talk();

    Person();

    static void  test_bind();


    void kk(int value);


public:
    //is static object ,编译时期进行初始化
    static Person person;

};


#endif //JTEST_PERSON_H
