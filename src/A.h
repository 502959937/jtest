//
// Created by chenlei on 2/1/16.
//

#ifndef JTEST_A_H
#define JTEST_A_H

#include "B.h"

#include <pthread.h>

/**
 *
 */
extern pthread_mutex_t pthreadMutexT;

//define class in b.h
extern class B;

class A {


public:
    B *_b;

    A(B *_b) : _b(_b) {
        pthread_mutex_lock(&pthreadMutexT);
        //todo::
        pthread_mutex_unlock(&pthreadMutexT);
    }

    /**
     * 析构函数 在A对象被删除的时候  进行自动调用
     */
    virtual ~A() {
        if (_b) {
            delete _b;
            _b = nullptr;
        }
    }

    /**
     *
     */
    static
    void *show(void *args);

};


#endif //JTEST_A_H
