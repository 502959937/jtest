//
// Created by chenlei on 1/24/16.
//

#include <thread>
#include "outstrm.h"

void outstrm::_show(long &value) {
    printf("get value:%li\n", value);
}

/**
 *
 */
void outstrm::Display(vector<long> &v, const char *s) {
    cout << endl << s << endl;

    //output stream
    copy(v.begin(), v.end(),
         ostream_iterator<long>(cout, "\t"));

    list<long> temp;

    //insert in list
    copy(v.begin(), v.end(), front_inserter(temp));

    //do some logic
    if (!temp.empty()) {
        for (auto item = temp.begin(); item != temp.end(); ++item) {
//            outstrm::show(*item);
        }

        for_each(temp.begin(), temp.end(), outstrm::_show);

        cout << endl;
    }

}


/**
 *  defence args struck
 *
 */
typedef struct {
    long value;
} ARGS;


void *outstrm::show(void *value) {
    ARGS *args = (ARGS *) value;
    printf("get value:%li \n", args->value);
    delete args;
}


void *outstrm::std_show(void *value) {
    ARGS *args = (ARGS *) value;
    printf("get value:%li \n", args->value);
}

/**
 *
 */
void outstrm::test_pthread() {

    test_std_thread();

    pthread_t __thread_id;

    ARGS *args = new ARGS;
    args->value = 1000;
    //static  method
    pthread_create(&__thread_id, NULL, &outstrm::show, args);
    pthread_join(__thread_id, NULL);
}

#include <thread>

void outstrm::test_std_thread() {
    ARGS args;
    args.value = 1000;
    std::thread t(&outstrm::std_show, &args);
    t.join();
}

