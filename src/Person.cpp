//
// Created by chenlei on 15/5/14.
//

#include "Person.h"

//static object
//init person namespace object ,static
Person Person::person;

void Person::say() {

    cout << "this is person method say!!" << endl;

}

//重写父类函数   多态性
void Person::talk() {
    cout << "this is person talk!!!" << endl;
}

Person::Person() {
    cout << "person:" << &person << endl;
}

void Person::test_bind() {

}

void Person::kk(int value) {

    /**
     *
     */
    printf("test boost bind function:%d\n",value);
}
