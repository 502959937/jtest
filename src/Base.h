//
// Created by chenlei on 15/5/14.
//

#ifndef JTEST_BASE_H
#define JTEST_BASE_H

#include "iostream"

#include "pthread.h"

static pthread_mutex_t pthreadMutexT;

using namespace std;

class Base {

public:

    //接口
    virtual void say() = 0;// abstrack class

    virtual void talk();

    virtual ~Base() { }
};

#endif //JTEST_BASE_H
