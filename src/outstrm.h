//
// Created by chenlei on 1/24/16.
//

#ifndef JTEST_OUTSTRM_H
#define JTEST_OUTSTRM_H

#include <iostream>
#include <time.h>      // Need time()
#include <algorithm>   // Need sort(), copy()
#include <vector>      // Need vector
#include <iterator>
#include <list>

using namespace std;


class outstrm {




public:

    void Display(vector<long> &v, const char *s);

    /**
     *
     */
    static void _show(long &value);

    static void *show(void *value);

    /**
     *
     */
    static void *std_show(void* value);


    void test_pthread();

    /**
     * std thread test
     */
    void test_std_thread();


};


#endif //JTEST_OUTSTRM_H
