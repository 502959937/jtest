/**
 * Created by 石头哥哥 on 1/23/16.
 */
//
// Created by chenlei on 1/23/16.
//

#ifndef JTEST_POINTER_H
#define JTEST_POINTER_H


#include <string.h>
#include <set>


/**
 * 1.指针类型 (存储值内存中地址)
 * 2.指向的数值类型
 */


// class  封装   ,继承  ,多态性 (虚函数)
class Pointer {


private:
    int *ptr_int;//int *  类型指针

    int a = 12;
    int b;
    int *p;
    int **ptr;
    int ***ptr_3; //into***类型的指针 ,int** (指向的类型)

    int array[10] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9}, value;

    /**
     *
     */
    char *str[5] = {
            (char *) "Hello,this is a sample!",
            (char *) "Hi,good morning.",
            (char *) "Hello world"
    };


    /**
     *
     */
     virtual  void  ptr_test();

public:
    /**
     *
     */
    void ptr_println();

public:
    Pointer(int *ptr_int) : ptr_int(ptr_int) {
        p = &a;
        ptr = &p;
        ptr_3 = &ptr;

        //string copy
        char s[80];
        //地址 接引
        strcpy(s, str[0]);//也可写成strcpy(s,*str);
        strcpy(s, str[1]);//也可写成strcpy(s,*(str+1));
        strcpy(s, str[2]);//也可写成strcpy(s,*(str+2));
    }
};


#endif //JTEST_POINTER_H
